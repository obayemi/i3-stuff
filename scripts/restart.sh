#!/bin/sh
## restart.sh for  in /home/beaune_a/.i3
##
## Made by 
## Login   <beaune_a@epitech.net>
##
## Started on  Thu Mar 20 21:06:08 2014 
## Last update Wed Apr  2 20:33:21 2014 
##

grep_result="`grep "#include file " ~/.i3/config`"
find="#include file : yes"

if [ "$grep_result" == "$find" ]
then
    cat ~/.i3/config.basic ~/.i3/include/* > ~/.i3/config
else
    cp ~/.i3/config.basic ~/.i3/config
fi
