#!/usr/bin/python
## changeKeyMap.py for  in /home/beaune_a/.i3
##
## Made by Amaury Beaune
## Login   <beaune_a@epitech.net>
##
## Started on  Wed Oct  2 01:12:55 2013 Amaury Beaune
## Last update Wed Oct  2 01:14:55 2013 Amaury Beaune
##

import os

if __name__ == '__main__':
    result = os.popen('setxkbmap -query').readlines()
    if len(result) == 3:
        os.system('setxkbmap us intl')
    else:
        os.system('setxkbmap us')
