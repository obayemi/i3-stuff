#!/usr/bin/python
## i3barPython.py for  in /home/beaune_a/.i3
##
## Made by Amaury Beaune
## Login   <beaune_a@epitech.net>
##
## Started on  Sat Oct 12 15:09:25 2013 Amaury Beaune
## Last update Wed Apr  2 23:29:15 2014 
##

import time
import os
import sys

normalColor = '#315858'
urgentColor = '#ff0000'

def ipAddr():
    """
    """
    networkActive = []
    conkyChangeIP = []
    resultIfconfig = os.popen('ifconfig').readlines()
    for line in resultIfconfig:
        if line.split(' ')[0] != '' and line.split(' ')[0] != '\n' and line.split(' ')[0] != 'lo':
            networkActive.append(line.split(':')[0])
    i = 0
    while i < len(networkActive):
        if networkActive[i] != 'lo':
            resultIfconfig = os.popen("ifconfig %s | grep 'inet ' | awk '{print $2}'" % networkActive[i]).readlines()
        if len(resultIfconfig) > 0 and networkActive[i] != 'lo':
            networkActive[i] = {'network' : networkActive[i], 'ip' : resultIfconfig[0].split('\n')[0]}
        else:
            networkActive.pop(i)
            i -= 1
        i += 1
    color = normalColor
    networkStr = ''
    for network in networkActive:
        networkStr += '{ "full_text" : "%s: %s" , "color" : "%s" } ,' % (network.get('network'), network.get('ip'), color)
    return networkStr

def battery():
    """
    """

    with open('/sys/class/power_supply/BAT0/status') as file:
        status =  file.readlines()
    with open('/sys/class/power_supply/BAT0/charge_now') as file:
        chargeNow =  int(file.readlines()[0][:-1])
    with open('/sys/class/power_supply/BAT0/charge_full') as file:
        chargeFull =  int(file.readlines()[0][:-1])
    percent = chargeNow * 100 / chargeFull
    color = normalColor
    if status[0] == 'Discharging\n' and percent < 16:
        color = urgentColor
    batteryStr = '{ "full_text" : "BAT: %c %.2f%%" , "color" : "%s" } ,' % (status[0][0], percent, color)
    return batteryStr

def cpu():
    """
    """
    allPercent = os.popen("ps -eo pcpu | sort -k 1 -r | head -20 | grep -v CPU").readlines()
    totalPercent = sum(map(float, allPercent))
    color = normalColor
    totalPercent /= 4
    with open('/proc/loadavg') as file:
        charge = file.readlines()[0].split()[0]
    if totalPercent >= 20:
        color = urgentColor
    cpuStr = '{ "full_text" : "CPU: %.2f%% (%s)" , "color" : "%s" } ,' % (totalPercent, charge, color)
    return cpuStr

def mem():
    """
    """
    total, used = map(int, os.popen("free").readlines()[1].split()[1:3])
    percent = used * 100 / total
    color = normalColor
    if percent >= 60 :
        color = urgentColor
    memStr = '{ "full_text" : "MEM: %.2f%%" , "color" : "%s" } ,' % (percent, color)
    return memStr

def fileSystemCheck():
    """
    """
    fileSystem = ''
    for i in os.popen('df -h').readlines():
        if i.startswith('/dev/'):
            name, size, used, aval, percent, mount = i.split()
            color = normalColor
            if int(percent[:-1]) >= 90 :
                color = urgentColor
            fileSystem += '{ "full_text" : "%s: %s %s" , "color" : "%s" } ,' % (mount, aval, percent, color)
    return fileSystem

def sound():
    """
    """
    line = os.popen("amixer get Master | awk -F'[]%[]' '/%/'").readlines()[0].split('\n')[0]
    if line.find('off') >= 0:
        percent = 'mute'
    else:
        percent = line.split(']')[0].split('[')[1]
    color = normalColor
    soundStr = '{ "full_text" : "LVL: %s" , "color" : "%s" } ,' % (percent, color)
    return soundStr

def netSoul():
    """
    """
    color = normalColor
    if os.path.exists('/tmp/tryExceptSoul') == False:
        color = urgentColor
    netsoulStr = '{ "full_text" : "NetSoul" , "color" : "%s" } ,' % (color)
    return netsoulStr

def temp():
    """
    """
    with open('/sys/bus/acpi/devices/LNXTHERM:00/thermal_zone/temp') as file:
        temp =  int(file.readlines()[0][:-1]) / 1000
    color = normalColor
    if temp >= 65 :
        color = urgentColor
    tempStr = '{ "full_text" : "TEMP: %sC" , "color" : "%s" } ,' % (temp, color)
    return tempStr

if __name__ == '__main__':
    i = 0
    sys.stdout.write('{"version":1}')
    sys.stdout.write('[')
    sys.stdout.write('[],')
    while 1:
        sys.stdout.write('[')
        sys.stdout.write(netSoul())
        sys.stdout.write(cpu())
        sys.stdout.write(temp())
        sys.stdout.write(mem())
        sys.stdout.write(fileSystemCheck())
        sys.stdout.write(ipAddr())
        sys.stdout.write(battery())
        sys.stdout.write(sound())
        sys.stdout.write('{ "full_text" : "%s" , "color" : "#315858" }' % time.strftime('%d-%m-%y %I:%M %P'))
        sys.stdout.write('],')
        sys.stdout.flush()
        time.sleep(2)
        i += 1
