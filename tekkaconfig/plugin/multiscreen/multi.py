#!/usr/bin/python
## multi.py for  in /home/beaune_a/.i3
##
## Made by Amaury Beaune
## Login   <beaune_a@epitech.net>
##
## Started on  Tue Oct  8 00:51:23 2013 Amaury Beaune
## Last update Thu Mar 20 20:50:49 2014 
##

import os
import sys

if __name__ == '__main__':
    returnXrandr = os.popen("xrandr").readlines()
    i = 0
    if len(sys.argv) == 2:
        pos = sys.argv[1]
    else:
        pos = 0
    output = 0
    connected = False
    while i < len(returnXrandr):
        if returnXrandr[i].split(' ')[0] != 'LVDS':
            if returnXrandr[i].split(' ')[1] == 'connected':
                output = returnXrandr[i].split(' ')[0]
                if returnXrandr[i + 1].split(' ')[9].find('*') < 0 and pos != 0:
                    connected = True
                elif returnXrandr[i + 1].split(' ')[9].find('*') >= 0:
                    connected = False
        i += 1
    if connected == True and pos != 0 and output != 0:
        os.system('xrandr --output  LVDS  --auto --primary --rotate normal --output %s --rotate normal --auto --noprimary %s LVDS' % (output, pos))
    elif output != 0:
        os.system('xrandr --output %s --off --output LVDS --auto' % output)
    else:
        os.system('xrandr --auto')
