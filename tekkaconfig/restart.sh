#!/bin/sh
## restart.sh for  in /home/beaune_a/.i3
##
## Made by 
## Login   <beaune_a@epitech.net>
##
## Started on  Thu Mar 20 21:06:08 2014 
## Last update Fri Mar 28 19:55:45 2014 
##

grep_result="`grep "#include file " ~/.i3/config`"
find="#include file : yes"
handling=$1
if [ "$grep_result" == "$find" ]
then
    cat ~/.i3/config.basic ~/.i3/include/* > ~/.i3/config
else
    cp ~/.i3/config.basic ~/.i3/config
fi
i3-msg $handling
