#!/usr/bin/env sh
## bat_alarm.sh for alarm on low bat in /home/eax/dev
## 
## Made by kevin soules
## Login   <soules_k@epitech.net>
## 
## Started on  Sat Jan 26 13:47:27 2013 kevin soules
## Last update Sat Jun 29 12:55:33 2013 Martial Elegbede
##

if [ `ps aux | grep bat_alarm | grep -v grep | wc -l` -ne 2 ]
then
    exit
fi

while true
do
    status=`cat /sys/class/power_supply/BAT0/status`

    charge_now=`cat /sys/class/power_supply/BAT0/charge_now`
    charge_full=`cat /sys/class/power_supply/BAT0/charge_full`

    percent_too_low=10
    percent_now_float=`echo "scale=3;$charge_now * 100 / $charge_full" | bc`
    percent_now=`echo "$percent_now_float/1" | bc`

    if [ $status = "Discharging" ]
    then
	if  [ $percent_now -le $percent_too_low ]
	then
	    i3-nagbar -t warning -m "Low battery !" -b "Suspend ?" "systemctl hybrid-sleep"
	fi
    fi
    sleep 60
done
